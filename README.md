# shell-scripts

All my handy shell scripts in one place!

These files are public to provide a general overview on how I build my own little menues to interact with my system.
Please note that all these files are heavily customized to my hardware and needs.
A few major changes need to be done to make them usable on another system (e.g. sink ids, hardware names, paths).

